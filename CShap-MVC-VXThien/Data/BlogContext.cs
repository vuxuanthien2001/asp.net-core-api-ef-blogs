﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CShap_MVC_VXThien.Data
{
    using Microsoft.EntityFrameworkCore;
    using CShap_MVC_VXThien.Models;

    public class BlogContext
        : DbContext
    {
        public BlogContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Blog> Blogs { get; set; }
    }
}
