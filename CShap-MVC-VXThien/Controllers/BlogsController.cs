﻿using CShap_MVC_VXThien.Data;
using CShap_MVC_VXThien.Models;
using CShap_MVC_VXThien.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CShap_MVC_VXThien.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogsController : ControllerBase
    {

        private readonly IBlogepository _BlogRepository;

        public BlogsController(IBlogepository BlogRepository)
        {
            _BlogRepository = BlogRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Blog>> GetBlogs()
        {
            return await _BlogRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Blog>> GetBlogs(string id)
        {
            return await _BlogRepository.Get(id);
        }

        [HttpPost]
        public async Task<ActionResult<Blog>> PostBlogs([FromBody] Blog Blog)
        {
            var newBlog = await _BlogRepository.Create(Blog);
            return CreatedAtAction(nameof(GetBlogs), new { id = newBlog.Id }, newBlog);
        }

        [HttpPut]
        public async Task<ActionResult> PutBlogs(string id, [FromBody] Blog Blog)
        {
            if (id != Blog.Id)
            {
                return BadRequest();
            }

            await _BlogRepository.Update(Blog);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var BlogToDelete = await _BlogRepository.Get(id);
            if (BlogToDelete == null)
                return NotFound();

            await _BlogRepository.Delete(BlogToDelete.Id);
            return NoContent();
        }
    }
}
