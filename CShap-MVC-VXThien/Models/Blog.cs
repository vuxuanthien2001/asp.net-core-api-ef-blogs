﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CShap_MVC_VXThien.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Blog")]
    public class Blog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        public string Title { get; set; }

        public string Describe { get; set; }

        public string Detail { get; set; }

        public string Image { get; set; }

        public string Location { get; set; }

        public bool? Status { get; set; }

        public string Type { get; set; }

        public DateTime? DatePublic { get; set; }

    }
}
