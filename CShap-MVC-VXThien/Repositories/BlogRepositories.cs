﻿using CShap_MVC_VXThien.Data;
using CShap_MVC_VXThien.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CShap_MVC_VXThien.Repositories
{
    public class BlogRepository : IBlogepository
    {
        private readonly BlogContext _context;

        public BlogRepository(BlogContext context)
        {
            _context = context;
        }

        public async Task<Blog> Create(Blog book)
        {
            _context.Blogs.Add(book);
            await _context.SaveChangesAsync();

            return book;
        }

        
        public async Task Delete(string id)
        {
            var bookToDelete = await _context.Blogs.FindAsync(id);
            _context.Blogs.Remove(bookToDelete);
            await _context.SaveChangesAsync();
        }

        

        public async Task<IEnumerable<Blog>> Get()
        {
            return await _context.Blogs.ToListAsync();
        }

        public async Task<Blog> Get(string id)
        {
            return await _context.Blogs.FindAsync(id);
        }

        

        public async Task Update(Blog blog)
        {
            _context.Entry(blog).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

       
    }
}