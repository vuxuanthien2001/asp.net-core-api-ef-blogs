﻿using CShap_MVC_VXThien.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CShap_MVC_VXThien.Repositories
{
    public interface IBlogepository
    {
        Task<IEnumerable<Blog>> Get();
        Task<Blog> Get(string id);
        Task<Blog> Create(Blog book);
        Task Update(Blog book);
        Task Delete(string id);
    }
}
